#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    
    def test_read_2(self):
        s = "10 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 10)
    
    def test_read_3(self):
        s = "5 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  5)
        self.assertEqual(j, 1)
    
    def test_read_4(self):
        s = "9999 99999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  9999)
        self.assertEqual(j, 99999)
    
    def test_read_5(self):
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 1)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_6(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    def test_eval_7(self):
        v = collatz_eval(1000, 1001)
        self.assertEqual(v, 143)

    def test_eval_8(self):
        v = collatz_eval(9999, 1)
        self.assertEqual(v, 262)

    def test_eval_9(self):
        v = collatz_eval(5, 5498)
        self.assertEqual(v, 238)

    def test_eval_10(self):
        v = collatz_eval(3, 9)
        self.assertEqual(v, 20)

    def test_eval_11(self):
        v = collatz_eval(10, 11)
        self.assertEqual(v, 15)

    def test_eval_12(self):
        v = collatz_eval(11, 10)
        self.assertEqual(v, 15)
    
    def test_eval_13(self):
        v = collatz_eval(800, 1)
        self.assertEqual(v, 171)
    
    def test_eval_14(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
    
    def test_eval_15(self):
        v = collatz_eval(5498, 5)
        self.assertEqual(v, 238)


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 10, 20)
        self.assertEqual(w.getvalue(), "100 10 20\n")
    
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 999, 90, 9)
        self.assertEqual(w.getvalue(), "999 90 9\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 937, 92347, 2345)
        self.assertEqual(w.getvalue(), "937 92347 2345\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    def test_solve_2(self):
        r = StringIO("1 10\n10 20\n21 21\n90 100\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n10 20 21\n21 21 8\n90 100 119\n")

    def test_solve_3(self):
        r = StringIO("1 1\n9 9\n201 2\n9 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n9 9 20\n201 2 125\n9 1000 179\n")

    def test_solve_4(self):
        r = StringIO("5 10\n10 15\n15 20\n20 25\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "5 10 20\n10 15 18\n15 20 21\n20 25 24\n")

    def test_solve_5(self):
        r = StringIO("10 20\n30 40\n40 50\n50 60\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 20 21\n30 40 107\n40 50 110\n50 60 113\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
